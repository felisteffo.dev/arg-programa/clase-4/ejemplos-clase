package ar.edu.utnfc.argprog;

import ar.edu.utnfc.argprog.singleton.Configuracion;

public class App
{
    public static void main( String[] args )
    {
        Configuracion ref = new Configuracion();
//        Configuracion ref = Configuracion.getInstancia();

        System.out.println("Propiedades del archivo de configuración");
        for(String propName : ref) {
            System.out.println(propName + " ==> " + ref.getValue(propName));
        }

    }
}
