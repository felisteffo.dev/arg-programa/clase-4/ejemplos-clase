package ar.edu.utnfc.argprog.singleton;

import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.TestMethodOrder;

import static org.junit.jupiter.api.Assertions.*;

//@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class ConfiguracionTest {

    private Configuracion referencia1;
    private Configuracion referencia2;

    @org.junit.jupiter.api.BeforeEach
    void setUp() {
        // Si estamos en presencia de un Singleton esto no compila
        referencia1 = new Configuracion();
        referencia2 = new Configuracion();

        // Si tomamos las intancias del Singleton usamos
//        referencia1 = Configuracion.getInstancia();
//        referencia2 = Configuracion.getInstancia();

    }

    /**
     * Chequear que de haber más de una instancia de Configuración siempre sean el mismo objeto en la memoria.
     */
    @org.junit.jupiter.api.Test
//    @Order(1)
    void getInstancia() {
        // Chequeamos que todas las instancias apunten al mismo objeto en memoria.
        assertTrue(referencia1 == referencia2, "Las instancias de configuración no apuntan al mismo objeto");

    }

    @org.junit.jupiter.api.Test
//    @org.junit.jupiter.params.ParameterizedTest
//    @org.junit.jupiter.params.provider.CsvSource(value = {"propiedad1:Valor1", "propiedad2:15", "propiedad3:True"}, delimiter = ':')
//    void getValue(String propertyName, String actual) {
//    @Order(2)
    void getValue() {

//        // Chequeamos la lectura de varias propiedades.
        assertEquals("Valor1", referencia1.getValue("propiedad1"));
        assertEquals("15", referencia1.getValue("propiedad2"));
        assertEquals("True", referencia1.getValue("propiedad3"));

//        assertEquals(actual, referencia1.getValue(propertyName));

    }

    @org.junit.jupiter.api.Test
//    @Order(3)
    void setValue() {
        // Probamos el establecimiento de valor nuevo de una propiedad
        referencia1.setValue("propiedad1", "ValorActualizado");

        // Chequeamos que efectivamente se haya actualizado
        assertEquals("ValorActualizado", referencia1.getValue("propiedad1"));

        // Chequeamos que todas las referencias vean el mismo valor actualizado
        assertEquals("ValorActualizado", referencia2.getValue("propiedad1"));
    }
}